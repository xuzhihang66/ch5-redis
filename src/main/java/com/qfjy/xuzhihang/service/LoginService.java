package com.qfjy.xuzhihang.service;

import java.util.Map;

/**
 * @author xzh
 * @title: loginService
 * @projectName ch5-redis
 * @description: TODO
 * @date 2021/8/6 17:23
 */
public interface LoginService {

    Map<String,Object> checkUserLogin(String username);

    Map<String,Object> checkPassword(String username,String password);
}
