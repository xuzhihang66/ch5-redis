package com.qfjy.xuzhihang.common;

/**
 * @author xzh
 * @title: UserKey
 * @projectName ch5-redis
 * @description: TODO
 * @date 2021/8/6 20:31
 */
public class RedisKey {

    public static String getUserLoginErrorCountKey(String username){
        return "user:"+username+":count";
    }

    public static String getUserPasswordKey(String username){
        return "user:"+username+":password";
    }

    public static String getUerLoginLock(String username){
        return "user:"+username+":lock";
    }
}
