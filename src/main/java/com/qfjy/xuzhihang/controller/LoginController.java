package com.qfjy.xuzhihang.controller;

import com.qfjy.xuzhihang.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author xzh
 * @title: LoginController
 * @projectName ch5-redis
 * @description: TODO
 * @date 2021/8/6 17:25
 */
@Controller
@RequestMapping("login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @GetMapping("toLogin")
    @ResponseBody
    public String login(@RequestParam(name="username") String username,
                        @RequestParam(name="password") String password){
        Map<String, Object> map = loginService.checkUserLogin(username);
        if ((Boolean)map.get("flag")==true){//被限制登录
            Long lockTime = (Long) map.get("lockTime");
            long time = lockTime+1L;
            return "您被限制登录，请"+time+"分钟后再试";
        } else {
            Map<String, Object> map1 = loginService.checkPassword(username, password);
            if ((Boolean)map1.get("flag")==true){
                return "登录成功";
            } else {
                if (map1.get("lock")!=null){
                    return "密码错误,请60分钟后再试";
                } else {
                    int count = (int) map1.get("count");
                    Long second = (Long) map1.get("second");
                    return "密码错误，"+second+"秒内您还有"+count+"次机会";
                }
            }
        }
    }
}
